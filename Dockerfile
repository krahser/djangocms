FROM ubuntu:14.04
MAINTAINER Aldo Maria Vizcaino <aldo.vizcaino87@gmail.com>

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update

ADD archivos/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN apt-get install -yqq\
    python2.7-dev\
    libxml2-dev\
    wget\
    sqlite3\
    python-pip\
    libmysqlclient-dev\
    mariadb-server\
    supervisor

RUN useradd djangocms
RUN mkdir /home/djangocms -p
RUN chown djangocms /home/djangocms
RUN locale-gen en_US.UTF-8 es_AR.UTF-8

RUN echo 'debconf debconf/frontend select Dialog' | debconf-set-selections
ENV LANG es_AR.UTF-8  
ENV LANGUAGE es_AR:es  
ENV LC_ALL es_AR.UTF-8


RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

VOLUME /home/djangocms/src

EXPOSE 8000

CMD /usr/bin/supervisord
